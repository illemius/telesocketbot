import string

from NucleusUtils.jProtocol.exceptions import APIException
from TeleSocketClient import types
from nucleusapp_telegram.static import ParseMode

import settings
from app import translate as _, App
from manager.dialogs import Dialog


def register(username, access_code=None):
    telesocket = App().registry.get_app_config('nucleusapp_telegram').telesocket
    try:
        user = telesocket.register(username, access_code)
    except APIException as e:
        return str(e.message)
    return user


class Registration(Dialog):
    paginate = True
    parse_mode = ParseMode.MARKDOWN

    def get_message(self):
        return _('Enter your username')

    def check_username(self, username):
        allowed = string.ascii_lowercase + string.digits + '_'
        for symbol in username.lower():
            if symbol not in allowed:
                return False
        return True

    def proceed(self, message):
        if not message.text or not self.check_username(message.text):
            self.send_message(_('Username must have been only alphabet characters or digits!', self.user.locale))
            return self.start()

        if settings.ACCESS_KEYS:
            return AccessCodeDialog(self.base_message, self).start()

        result = register(message.text)
        if isinstance(result, types.User):
            result = _('You access token for TeleSocket service: `{{ token }}`.',
                       self.user.locale, env={'token': result.token})
        else:
            result = _(result, self.user.locale)
        self.done(result)


class AccessCodeDialog(Dialog):
    paginate = True
    parse_mode = ParseMode.MARKDOWN

    def get_message(self):
        return _('Enter access key', self.user.locale)

    def proceed(self, message):
        if not message.text:
            return self.start()

        result = register(self.previous.message.text, message.text)
        if isinstance(result, types.User):
            result = _('You access token for TeleSocket service: `{{ token }}`.',
                       self.user.locale, env={'token': result.token})
        else:
            result = _(result, self.user.locale)
        self.done(result)
