import secrets

from nucleusapp_telegram.base import bot
from telebot import types

from app import translate as _
from manager.models import User
from manager.utils import handle_exceptions

in_dialog = []


class Dialog:
    reference = secrets.token_hex(4)
    replying = False
    parse_mode = None
    paginate = False

    def __init__(self, message, previous=None, step_message=None):
        self.base_message = message
        self.previous = previous
        self.step_message = step_message

        self.id = secrets.token_hex(3)
        self.user = User.get_user(message.chat.id)
        self.message: types.Message = None

    def send_message(self, text, reply_to=None, reply_markup=None):
        if reply_to is None and self.replying:
            reply_to = self.base_message
        if reply_to:
            return bot.reply_to(reply_to, text, reply_markup=reply_markup, parse_mode=self.parse_mode)
        return bot.send_message(self.base_message.chat.id, text, reply_markup=reply_markup, parse_mode=self.parse_mode)

    def start(self):
        if self.user.id not in in_dialog:
            in_dialog.append(self.user.id)

        self.message = None
        message = self.send_message(self.get_message(), reply_markup=self.get_markup())
        bot.register_next_step_handler(message, self.on_message)

    def cancel(self):
        self.send_message(_('Canceled.', self.user.locale), reply_markup=types.ReplyKeyboardRemove())

    def done(self, text=None):
        return self.send_message(text or _('Done', self.user.locale), reply_markup=types.ReplyKeyboardRemove())

    def back(self):
        if self.previous:
            self.previous.start()

    def get_message(self):
        raise NotImplementedError

    def get_markup(self):
        markup = self.get_buttons()
        if not markup and self.paginate:
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True)

        if self.paginate:
            if self.previous:
                markup.add(_('Back', self.user.locale), _('Cancel', self.user.locale))
            else:
                markup.add(_('Cancel', self.user.locale))

        return markup

    def get_buttons(self):
        return None

    @handle_exceptions
    def on_message(self, message):
        if self.user.id in in_dialog:
            in_dialog.remove(self.user.id)

        if message.text == _('Back', self.user.locale):
            self.back()
        elif message.text == _('Cancel', self.user.locale):
            self.cancel()
        else:
            self.message = message
            self.proceed(message)

    def proceed(self, message):
        pass
