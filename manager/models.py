import datetime

from mongoengine import *

import settings
from . import log


class User(Document):
    id = IntField()
    locale = StringField(default=settings.DEFAULT_LOCALE)
    root = BooleanField(default=False)
    # Logging
    creation_date = DateTimeField(default=datetime.datetime.now)
    modified_date = DateTimeField(default=datetime.datetime.now)

    def set_root(self, value):
        self.root = value
        self.save()

    def is_root(self):
        return self.root

    def save(self, *args, **kwargs):
        if not self.creation_date:
            self.creation_date = datetime.datetime.now()
        self.modified_date = datetime.datetime.now()
        updated = self._get_changed_fields()
        if len(updated):
            log.info(f"Update dialog {self.id}: {self._get_changed_fields()}")
        else:
            fields = {key: getattr(self, key) for key in self._fields.keys()}
            log.info(f"Register dialog {fields}")
        return super(User, self).save(*args, **kwargs)

    @classmethod
    def get_user(cls, chat_id) -> 'User':
        dialogs = User.objects(id=chat_id)
        if len(dialogs):
            return dialogs[0]
        return User(id=chat_id)
