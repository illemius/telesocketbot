from nucleusapp_telegram.base import bot

from app import translate as _
from manager.dialogs.register import Registration
from manager.utils import handle_exceptions


@bot.message_handler(['start'])
def cmd_start(message):
    bot.reply_to(message, _('Hello!'))


@bot.message_handler(['register'])
@handle_exceptions
def cmd_test(message):
    Registration(message).start()
