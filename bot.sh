#!/usr/bin/env bash
cd "$(dirname "$0")"

INIT_CMD="python3.6 ."
PID_FILE=".pid"

function start {
    ${INIT_CMD}
}

function background {
    if  [ -f "$PID_FILE" ]; then
        PID=`cat ${PID_FILE}`
        if ps -p ${PID} > /dev/null; then
            echo "TeleSocket already started! " >&2
            quit
        fi
    fi
    ${INIT_CMD} > /dev/null 2>&1 &
    PID=$!
    echo "[$PID] Started."
    echo ${PID} > ${PID_FILE}
    quit
}

function stop {
    if ! [ -f "$PID_FILE" ]; then
        echo "PID file not found."
    else
        PID=`cat ${PID_FILE}`
        if ! kill ${PID} > /dev/null 2>&1; then
            echo "Could not send SIGINT to process $PID" >&2
        else
            echo "[$PID] Killed."
            rm ${PID_FILE}
        fi
    fi
    quit
}

function kill_process {
    if ! [ -f "$PID_FILE" ]; then
        echo "PID file not found."
    else
        PID=`cat ${PID_FILE}`
        if ! kill ${PID} > /dev/null 9>&1; then
            echo "Could not send SIGTERM to process $PID" >&2
        else
            echo "[$PID] Killed."
            rm ${PID_FILE}
        fi
    fi
    quit
}

function compress_logs {
    local count=$(ls -l dumps/ | grep .log | wc -l)
    if [ ${count} -gt '0' ]; then
        local now=$(date +%Y_%m_%d_%H_%M_%S)
        local filename="reports/backup_${now}.tar.gz"
        tar -czvf ${filename} reports/*.log
        rm -rf reports/*.log
        echo "Done! Generate archive '$filename'"
    else
        echo "Logs not found!"
    fi
}


function update_from_remote {
    git checkout .
    git pull
}

function quit {
    exit
}

case $1 in
    start)
        start
    ;;
    background|bg)
        background
    ;;
    stop)
        stop
    ;;
    kill)
        kill_process
    ;;
    compress)
        compress_logs
    ;;
    update)
        update_from_remote
    ;;
    full_update)
        update_from_remote
        sudo bash update_libs.sh
    ;;
    *)
        echo "Usage: '$0 start|background|stop|compress|update|full_update'"
        quit
    ;;
esac