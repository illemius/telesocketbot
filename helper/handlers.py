from nucleusapp_telegram.base import bot
from telebot import types

from app import translate as _
from manager.dialogs import in_dialog
from manager.models import User


def check_cancel(message):
    user = User.get_user(message.chat.id)
    return message.text and message.chat.id not in in_dialog and message.text == _('Cancel', user.locale)


@bot.message_handler(func=check_cancel)
def cancel(message):
    bot.reply_to(message, _('Canceled.', User.get_user(message.chat.id).locale),
                 reply_markup=types.ReplyKeyboardRemove())


@bot.message_handler(func=lambda message: message.chat.id not in in_dialog)
def unknown_messages(message):
    bot.reply_to(message, _('I do not know that.\nTry to read the /help', User.get_user(message.chat.id).locale),
                 reply_markup=types.ReplyKeyboardRemove())
