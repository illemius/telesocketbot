DEBUG = True

TIME_ZONE = 'Europe/Kiev'
DEFAULT_LOCALE = 'en'

DATABASE = {
    "active": "testing" if DEBUG else "production",
    "engines": {
        "testing": {
            "db": "telesocket_debug_db",
        },
        "production": {
            "db": "telesocket_db"
        }
    }
}

TELEGRAM = {
    "TOKEN": "TOKEN",
    "TELESOCKET_TOKEN": "TOKEN",
    "TELESOCKET_MANAGER": True,
    "SKIP_PENDING": True,
    "ROOT_UID": 1234567,
}

ACCESS_KEYS = False
