import atexit
import inspect
import os
import sys
import time

from NucleusApp.app import Application
from NucleusUtils.versions import Version
from mongoengine import connect


class App(Application):
    name = 'TeleSocketBot'
    version = Version(0, 1, 0)

    logging_config = 'logs/logging.json'

    modules = [
        'nucleusapp_telegram',
        'manager',
        'helper'
    ]

    def prepare(self):
        self.log.info(self.about())
        self.setup_time()
        self.setup_db()

    @staticmethod
    @atexit.register
    def on_exit():
        """
        Exit event
        :return:
        """
        time.sleep(1)
        print('Shutdown. Bye!')

    def setup_time(self):
        if not self.ready:
            if sys.platform != 'win32':
                # Note: in Win32 time has not attribute .tzset()
                os.environ['TZ'] = self.settings.get('TIMEZONE', 'utc')
                time.tzset()

    def setup_db(self):
        db_settings = self.settings.get('DATABASE')
        active = db_settings.get('active')
        if active:
            self.log.info(f"Connect to MongoDB '{active}' storage.")
            connect(**db_settings.get('engines', {}).get(active))

    def set_logging(self):
        # Correction a curved logger
        # First import this shit
        import telebot
        # And call something like this or any other func or e. g.
        telebot.util.is_string('')

        super(App, self).set_logging()


def translate(word, locale=None, env=None, module=None):
    if env is None:
        env = {}
    if module is None:
        frame = inspect.stack()[1]
        module = inspect.getmodule(frame[0]).__name__
    return App().translator.translate(word, locale, env, module)
